# View Marquee

This module using for marquee direction.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/view_marquee).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/view_marquee).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

Create View Marquee Style.


## Maintainers

- Deepak Bhati - [heni_deepak](https://www.drupal.org/u/heni_deepak)
- Radheshyam Kumawat - [radheymkumar](https://www.drupal.org/u/radheymkumar)